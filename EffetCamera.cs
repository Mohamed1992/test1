﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffetCamera : MonoBehaviour {
	KeyController carController ;
	public Transform car;
	float z;
	float x;



	public float positionSmoothTime = 0.7f;
	// Use this for initialization
	void Start () {
		
		carController = GameObject.Find ("Cube3").GetComponent<KeyController> ();
		z = this.transform.localPosition.z;
		x = this.transform.localPosition.x;
	
	}
	
	// Update is called once per frame
	void Update () {



		float positionSmoothDumpVelocity = 0;
		float positionSmoothDumpVelocity2 = 0;
		float x1 = Mathf.SmoothDamp (this.transform.localPosition.x, x +  carController.rotation, ref positionSmoothDumpVelocity, 0.1f);
		float z1 = Mathf.SmoothDamp (this.transform.localPosition.z, z -   carController.translation * 0.1f, ref positionSmoothDumpVelocity2, 0.1f);

		this.transform.localPosition = new Vector3(x1 , 0 , z1);


		this.transform.rotation = Quaternion.LookRotation (new Vector3(car.transform.position.x - this.transform.position.x ,
			0 , 
		car.transform.position.z - this.transform.position.z));
		
	}





}
