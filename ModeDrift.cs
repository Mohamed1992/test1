﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeDrift : MonoBehaviour {

	CarController3 carController ;
  

	public Transform BodyCar;
	float somme = 0;
	public Transform PointerLeft;
	public Transform PointerRight;
	public Transform PointerMotor; 

	float coeff1 = 0.01f;
	float coeff = 0.01f;

	void Start () {
        carController = GameObject.Find ("Cube3").GetComponent<CarController3> ();

		}



	void Update () {
		if (carController.isDrift){
			float translationSmoothDumpVelocity = 0f;
			 coeff1 = Mathf.SmoothDamp (coeff1, coeff, ref translationSmoothDumpVelocity, 0.1f);
			traitementCollisionModeDrift  (carController.speed,  carController.rotate , coeff1);
          }
	}


	public void setCoeff(float coeff2){
		this.coeff = coeff2;
    }

	public void traitementCollisionModeDrift (float speed , float rotate ,float coefficientDrift ) {

		this.transform.Translate (0, 0, speed * Time.deltaTime);

		if (Mathf.Abs (speed) < 2) 
			rotate = 0;  

	


		BodyCar.transform.Rotate (0, rotate * Time.deltaTime * 100 , 0);

		rotationCarAndBodyCar(coefficientDrift);

		BodyCar.transform.position = this.transform.position;



	}



	public void rotationCarAndBodyCar(float coefficient){
		float angle = Quaternion.Angle (this.transform.rotation, BodyCar.transform.rotation);


		if (Vector3.Distance (PointerLeft.transform.position, PointerMotor.transform.position) > Vector3.Distance (PointerRight.transform.position, PointerMotor.transform.position))
			angle = Mathf.Abs(angle) * -1 * coefficient * Time.deltaTime * 100;
		else
			angle = Mathf.Abs(angle) * coefficient *Time.deltaTime * 100;

		this.transform.eulerAngles = new Vector3 (0, this.transform.eulerAngles.y + angle, 0);


	}

}


/*
	 * 
	 * 
	 * 
	 * public float  traitementCollisionModeNormale (float speed , float rotate ) {

		CubeController.transform.Translate (0, 0, speed * Time.deltaTime);
		float distance  =  managerCollider.PointIntersectionTranslation();

		if (distance != 5555 ) {
			CubeController.transform.position =	this.transform.position;

			if (speed > 0) {
				CubeController.transform.Translate (0, 0, distance);

			} else if (speed < 0){
				CubeController.transform.Translate (0, 0, - distance);

			}

			speed = 0;
		} 


		if(Mathf.Abs(speed) < 2)
			rotate = rotate * (Mathf.Abs(speed) * 0.5f);



		CubeController.transform.Rotate (0, rotate * Time.deltaTime * 100, 0);

		
		
		if (!managerCollider.PointIntersectionRotation ()) {
			this.transform.rotation = CubeController.transform.rotation;
         } else {
			CubeController.transform.rotation = this.transform.rotation;	
		}


	this.transform.position = CubeController.transform.position;
	BodyCar.transform.position = this.transform.position;
	BodyCar.transform.rotation = this.transform.rotation;
	return speed;

}

	public float  traitementCollisionModeDrift (float speed , float rotate ,float coefficientDrift ) {

		CubeController.transform.Translate (0, 0, speed * Time.deltaTime);

		CubeController.transform.rotation = BodyCar.transform.rotation;

		float distance  =  managerCollider.PointIntersectionTranslation();

		if ( distance != 5555 ) {
			CubeController.transform.position =	this.transform.position;

			if (speed > 0) {
				CubeController.transform.Translate (0, 0, distance);

			} else if (speed < 0){
				CubeController.transform.Translate (0, 0, - distance);

			}

			speed = 0;
		} 

		if(Mathf.Abs(speed) < 2)
			rotate = rotate * (Mathf.Abs(speed) * 0.5f);


		CubeController.transform.Rotate (0, rotate * Time.deltaTime * 100 , 0);

		
	
		if (!managerCollider.PointIntersectionRotation ()) {
			BodyCar.transform.rotation = CubeController.transform.rotation;
			} else {
			speed = 0;
			}


		rotationCarAndBodyCar(coefficientDrift);

		this.transform.position = CubeController.transform.position;
		CubeController.transform.rotation = this.transform.rotation;
		BodyCar.transform.position = this.transform.position;

		return speed;

	}
*/
