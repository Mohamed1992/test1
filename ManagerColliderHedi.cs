﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerColliderHedi : MonoBehaviour {

	public Transform CubeController;
	public Transform[] pointsOfCollider;
	public Transform[] pointsofMur;
	Vector2[] S = new Vector2[4];



	public Transform[] pointsOfVecteursNormale = new Transform[8];
	Vector3[] VectersNormale = new Vector3[4];

	void Start () {

		int z = 0;

		for (int i = 0; i < 4; i++) {
			VectersNormale [i] = new Vector3 (pointsOfVecteursNormale[z+1].transform.position.x - pointsOfVecteursNormale[z].transform.position.x , 0 , pointsOfVecteursNormale[z+1].transform.position.z - pointsOfVecteursNormale[z].transform.position.z);
			z = z + 2;
		}




		S[0] = new Vector2 (pointsofMur[1].transform.position.x - pointsofMur[0].transform.position.x, pointsofMur[1].transform.position.z - pointsofMur[0].transform.position.z);
		S[1] = new Vector2 (pointsofMur[3].transform.position.x - pointsofMur[2].transform.position.x, pointsofMur[3].transform.position.z - pointsofMur[2].transform.position.z);
		S[2] = new Vector2 (pointsofMur[5].transform.position.x - pointsofMur[4].transform.position.x, pointsofMur[5].transform.position.z - pointsofMur[4].transform.position.z);
		S[3] = new Vector2 (pointsofMur[7].transform.position.x - pointsofMur[6].transform.position.x, pointsofMur[7].transform.position.z - pointsofMur[6].transform.position.z);
	

	}



	public float PointIntersectionTranslation(){
		
		Vector2[] tabPointCar = new Vector2[8]; 
		float distance = 5555;
		int posMur = 0;

		for(int i = 0 ; i<4 ; i++){
			tabPointCar[i] =new Vector2( pointsOfCollider[i].transform.position.x , pointsOfCollider[i].transform.position.z);
			tabPointCar[i+4] =new Vector2( pointsOfCollider[i+4].transform.position.x , pointsOfCollider[i+4].transform.position.z);

			Vector2 R = new Vector2 (tabPointCar [i + 4].x - tabPointCar [i].x, tabPointCar [i + 4].y - tabPointCar [i].y);



			int k = 0;
			for (int j = 0; j < 4; j++) {
				Vector2 CA = new Vector2 (pointsofMur[k].transform.position.x - tabPointCar [i].x, pointsofMur[k].transform.position.z - tabPointCar [i].y);

				float 	 pointIntersection2 = DeterminantMatrice (CA, S[j]) / DeterminantMatrice (R, S[j]);
				if (  Mathf.Abs(pointIntersection2) < 1 ) {
					Vector2 point = new Vector2 (tabPointCar [i].x + pointIntersection2 * R.x, tabPointCar [i].y + pointIntersection2 * R.y);
					float distance2 = Vector2.Distance (tabPointCar [i], point);
					if (distance2 < distance) {
						distance = distance2;
						posMur = j;
					}
				}
				k = k + 2;
			}



		}

return distance ;

	}

public	bool PointIntersectionRotation(){

		Vector2[] tabPointCar = new Vector2[4]; 

		for (int i = 0; i < 4; i++) {
			tabPointCar[i] =new Vector2( pointsOfCollider[i+4].transform.position.x , pointsOfCollider[i+4].transform.position.z);	
		}

		for(int i = 0 ; i<4 ; i++){

			Vector2 R;
			if(i<3)
				R = new Vector2 (tabPointCar [i+1].x - tabPointCar [i].x, tabPointCar [i + 1].y - tabPointCar [i].y);
			else
				R = new Vector2 (tabPointCar [0].x - tabPointCar [3].x, tabPointCar [0].y - tabPointCar [3].y);

			int k = 0;
			for (int j = 0; j < 4; j++) {
				Vector2 CA = new Vector2 (pointsofMur[k].transform.position.x - tabPointCar [i].x, pointsofMur[k].transform.position.z - tabPointCar [i].y);
					float 	 pointIntersection2 = DeterminantMatrice (CA, S[j]) / DeterminantMatrice (R, S[j]);
			if (  pointIntersection2 < 1 &&  pointIntersection2 > 0 ) {
						setPositionWithNormale (j);
					return true;
				}
				k = k + 2;
			}



		}


		return false;

	}


	float DeterminantMatrice(Vector2 A , Vector2 B){

		return A.x * B.y - A.y * B.x ;

	}

	public void setPositionWithNormale(int i){
		CubeController.transform.position = new Vector3 (CubeController.transform.position.x + 0.01f * VectersNormale[i].x,this.transform.position.y,CubeController.transform.position.z + 0.01f * VectersNormale[i].z );
	}



}






