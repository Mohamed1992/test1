﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeNormale : MonoBehaviour {
	CarController3 carController ;
	public Transform BodyCar;
	// Use this for initialization
	void Start () {
		carController = GameObject.Find ("Cube3").GetComponent<CarController3> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (!carController.isDrift)
			traitementCollisionModeNormale (carController.speed, carController.rotate * 0.6f);
	
	}

	public void  traitementCollisionModeNormale (float speed , float rotate ) {

		this.transform.Translate (0, 0, speed * Time.deltaTime);

		if (Mathf.Abs (speed) < 1) 
			rotate = 0; 



		this.transform.Rotate (0, rotate * Time.deltaTime * 100, 0);

		BodyCar.transform.position = this.transform.position;
		BodyCar.transform.rotation = this.transform.rotation;


	}
}
