﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurCercle : MonoBehaviour {
	public Transform[] pointsOfCollider;
	public Transform CenterCercle ;
	public Transform PointRayon ;
	Vector3 posReset ;
	Vector3 poscenter;
	float distance ;
	// Use this for initialization
	void Start () {
		poscenter = CenterCercle.transform.position;
		distance = Vector3.Distance (	poscenter , PointRayon.transform.position);
		posReset = GameObject.Find ("Cube3").transform.position;

	}
	
	// Update is called once per frame
	void Update () {

		for (int i = 0; i < 4; i++) {
				if (Vector3.Distance (CenterCercle.transform.position, pointsOfCollider [i].transform.position) < distance) {
				GameObject.Find ("Cube3").transform.position = posReset;
				GameObject.Find ("Cube3").GetComponent<CarController3> ().speed = 0;
             }

		}
		
	}
}
