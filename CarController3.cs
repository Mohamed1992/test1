﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.CrossPlatformInput;

public class CarController3 : MonoBehaviour {
	
	KeyController keyInput ;
	public float speed = 0;
	public int vitesse = 0;
	int vitessePrecedent; 
	public float speedMax = 3;
	float Distancevitesse;


	public float acceleration = 0.1f;
	ModeDrift modeDrift;

	public bool isDrift = false;

	public float translation = 0;
	public float rotate= 0;


	AudioSource SoundMotor ;


	// Use this for initialization


	void Start(){
		keyInput = this.GetComponent<KeyController> ();
		Distancevitesse = speedMax / 5;
		modeDrift = this.GetComponent<ModeDrift> ();

		StartCoroutine (TraitementUpdate ());

		SoundMotor = GameObject.Find ("bodyCarCollider").GetComponent<AudioSource> ();

	
	}

	void Update(){
		float vitesse1 = speed / Distancevitesse;
		vitesse = Mathf.RoundToInt( vitesse1 );
		if (vitesse > vitesse1)
			vitesse -= 1;


		translation = keyInput.translation;
		rotate =  keyInput.rotation;


		SoundMotor.pitch = 0.3f + Mathf.Abs (Distancevitesse * vitesse - speed) / 30 + Mathf.Abs (translation);


        float translationSmoothTime2 = 0.4f;
	
		if (isDrift) {
			
			traitementDrift ();
        }
	
//if ( CrossPlatformInputManager.GetButton("Jump")) {

      if ( Input.GetKey(KeyCode.Space)) {
			traitementInputSpace();
			translationSmoothTime2 = 0.2f;
		} else
			GameObject.Find ("CageVoiture").GetComponent<AudioSource> ().enabled = false;

	
		if (speed > speedMax || Mathf.Abs (translation) != 1) {
			float translationSmoothDumpVelocity2 = 0f;
			speed = Mathf.SmoothDamp (speed, 0, ref translationSmoothDumpVelocity2, translationSmoothTime2);
	
		} else { 
			
            if(speed > -4f)
				speed += acceleration * translation * Time.deltaTime * 100;
			
		}

} 


	void traitementDrift(){
		if (Mathf.Abs (translation) < 0.3f) 
		modeDrift.setCoeff (0.1f);
		else
		modeDrift.setCoeff (0.01f);

		if (vitesse > vitessePrecedent) {
			translation = 0;
			speed -= 20 * acceleration;
            vitesse = vitessePrecedent;
		} else if (vitesse < vitessePrecedent)
			vitessePrecedent = vitesse;
}


	void traitementInputSpace(){
		
		if(speed > 0)
			GameObject.Find ("CageVoiture").GetComponent<AudioSource> ().enabled = true;


		vitessePrecedent = vitesse;
		if (!isDrift) {
			isDrift = true;
			StartCoroutine (ControlleDrift ());
		}

	}

	private IEnumerator ControlleDrift(){
		GameObject.Find ("roue1").GetComponent<TrailRenderer> ().enabled = true;
		GameObject.Find ("roue2").GetComponent<TrailRenderer> ().enabled = true;

		while (isDrift) {
			yield return new WaitForSeconds (0.5f);
			float angle = Quaternion.Angle(this.transform.rotation , GameObject.Find("bodyCarCollider").transform.rotation);
			if(Mathf.Abs(angle) < 5f &&  translation < 0.5f )
				isDrift = !isDrift ;
			

		}

		GameObject.Find ("roue1").GetComponent<TrailRenderer> ().enabled = false;
		GameObject.Find ("roue2").GetComponent<TrailRenderer> ().enabled = false;

	}



	private IEnumerator TraitementUpdate(){

		bool isArieur = false;
		while (true) {
			yield return new WaitForSeconds (0.5f);

			if (Mathf.Abs (speed) < 1 && Mathf.Abs(translation) < 1) 
				speed = 0; 

			if (speed < 0) {
				
				if (!isArieur) {
					isArieur = true;
					GameObject.Find ("Main Camera").transform.localPosition = new Vector3 (0, 1.31f, 20f);
					GameObject.Find ("Main Camera").transform.localEulerAngles = new Vector3 (4.69f, 180f, 0);
				}
            } else {
				if (isArieur) {
					isArieur = false;

					GameObject.Find ("Main Camera").transform.localPosition = new Vector3 (0, 1.31f, 1f);
					GameObject.Find ("Main Camera").transform.localEulerAngles = new Vector3 (4.69f, 0, 0);
				}

			}
				
		}


	}




}