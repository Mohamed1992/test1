﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelText : MonoBehaviour {
	public Text textSpeed;
	public Text textVitesse;
	public Text textTime;
	public Text textCompteurFps;
	public Text textCompteurFps2;
	public float timeStart = 60;
	int compteurfps = 0;
	int compteurfps2 = 0;
	CarController3 car ;
	// Use this for initialization
	void Start () {
		car = GameObject.Find ("Cube3").GetComponent<CarController3> ();
		textTime.text = timeStart.ToString ();
		textCompteurFps.text = compteurfps.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		 
		textSpeed.text = "" +  Mathf.RoundToInt( car.speed); 
		textVitesse.text = "" + (car.vitesse+1);
		timeStart -= Time.deltaTime;
		textTime.text = Mathf.Round (timeStart).ToString ();
	
		if (1 / Time.deltaTime < 50) {
			compteurfps++;
			textCompteurFps.text = compteurfps.ToString ();
		}

		if (1 / Time.deltaTime < 55) {
			compteurfps2++;
			textCompteurFps2.text = compteurfps2.ToString ();
		}
			
	}
}
