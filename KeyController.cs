﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class KeyController : MonoBehaviour {
	public float translation = 0;
	public float rotation = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		translation = Input.GetAxis ("Vertical");
		rotation =  Input.GetAxis("Horizontal");

	}
}
