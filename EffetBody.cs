﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffetBody : MonoBehaviour {
	CarController3 carController ;
	KeyController carController2 ;
	float vitesse = 0;
	public Transform rouer1;
	public Transform rouer2;
	bool normale = true;
	float y;
	float z;

	// Use this for initialization
	void Start () {
		carController2 = GameObject.Find ("Cube3").GetComponent<KeyController> ();
		carController = GameObject.Find ("Cube3").GetComponent<CarController3> ();
		z = 180f;
		y = this.transform.localEulerAngles.x;
		}

	// Update is called once per frame
	void Update () {
		float translation = carController2.translation;
		float rotation = carController2.rotation;

        float positionSmoothDumpVelocity = 0;

		if (carController.vitesse != vitesse) {
			vitesse = carController.vitesse;
			normale = false;
			StartCoroutine (waitSeconde());
		}

		if(!normale)
			translation = 0f;

	float y1 = Mathf.SmoothDamp (this.transform.localEulerAngles.x, y - 2 * translation   , ref positionSmoothDumpVelocity, 0.05f);

		float z1 = 0;

		if(carController.speed < 0)
			z1 = z - 40 * rotation;
		else
			z1 = z + 40 * rotation;


		rouer1.transform.localEulerAngles = new Vector3 (0,z1, 0);
		rouer2.transform.localEulerAngles = new Vector3 (0,z1, 0);
		this.transform.localEulerAngles = new Vector3 ( y1 , 0 , 90f  );
	}

	private IEnumerator waitSeconde(){
			yield return new WaitForSeconds (0.1f);
		normale = true;
    }

		


}
