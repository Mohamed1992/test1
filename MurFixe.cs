﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurFixe : MonoBehaviour {
	public Transform[] pointsOfCollider;
	public Transform[] pointsofMur;
	Vector2 S ;
	 Vector3 posReset ;
	// Use this for initialization
	void Start () {
		S = new Vector2 (pointsofMur[1].transform.position.x - pointsofMur[0].transform.position.x, pointsofMur[1].transform.position.z - pointsofMur[0].transform.position.z);
		posReset = GameObject.Find ("Cube3").transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		Vector2[] tabPointCar = new Vector2[4]; 

		for (int i = 0; i < 4; i++) {
			tabPointCar[i] =new Vector2( pointsOfCollider[i].transform.position.x , pointsOfCollider[i].transform.position.z);	
		}

		for(int i = 0 ; i<4 ; i++){

			Vector2 R;
			if(i<3)
				R = new Vector2 (tabPointCar [i+1].x - tabPointCar [i].x, tabPointCar [i + 1].y - tabPointCar [i].y);
			else
				R = new Vector2 (tabPointCar [0].x - tabPointCar [3].x, tabPointCar [0].y - tabPointCar [3].y);

		
				
			Vector2 CA = new Vector2 (pointsofMur[0].transform.position.x - tabPointCar[i].x, pointsofMur[0].transform.position.z - tabPointCar [i].y);
			float 	 pointIntersection2 = DeterminantMatrice (CA, S) / DeterminantMatrice (R, S);

			Vector2 AC = new Vector2 (tabPointCar[i].x - pointsofMur[0].transform.position.x  , tabPointCar [i].y - pointsofMur[0].transform.position.z  );
			float 	 pointIntersection1 = DeterminantMatrice (AC, R) / DeterminantMatrice (S, R);

			if (  pointIntersection2 < 1 &&  pointIntersection2 > 0 ) {
				if (pointIntersection1 < 1 && pointIntersection1 > 0) {
					GameObject.Find ("Cube3").transform.position = posReset;
					GameObject.Find ("Cube3").GetComponent<CarController3> ().speed = 0;
				}
				}

			
    	}



	}

	float DeterminantMatrice(Vector2 A , Vector2 B){

		return A.x * B.y - A.y * B.x ;

	}

}
