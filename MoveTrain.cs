﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrain : MonoBehaviour {
	public Transform pointFin;
	public Transform pointDepart;
	public float Vitesse = 1; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate (0, 0, Vitesse * Time.deltaTime); 
		if (this.transform.position.z < pointFin.transform.position.z)
			this.transform.position = new Vector3(this.transform.position.x , this.transform.position.y , pointDepart.transform.position.z); 


	}
}
